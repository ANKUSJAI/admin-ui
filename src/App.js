import React, { Component } from 'react';
import logo from './logo-menu.png';
import './App.css';
import Search from './com/meanuasap/admin/components/search/Search';
import Update from './com/meanuasap/admin/components/search/Update';
import Restraurants from './com/meanuasap/admin/components/Restraurants';
class App extends Component {

  constructor(){
    super();
    this.state = {
      restraurants: [
      ],
      listDataFromChild: []
    }
  }

  myCallback = (dataFromChild) => {
    this.setState({ listDataFromChild: [dataFromChild] });
    this.setState({restraurants:this.state.listDataFromChild});
     }

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <Search callbackFromParent={this.myCallback}/>
        <p className="App-intro">
           MENU ASAP
        </p>
        <Restraurants restraurants={this.state.listDataFromChild}/>
        <Update restraurants={this.state.restraurants}/>
      </div>
    );
  }
}

export default App;
