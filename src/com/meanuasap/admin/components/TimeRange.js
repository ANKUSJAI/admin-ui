import React, { Component } from 'react';

class TimeRange extends Component {
  constructor(props) {
  super(props);
  if(this.props.timeRange){
    this.state = {
       timeRange: {
         name: this.props.timeRange.name,
         startTime:this.state.timeRange.startTime,
         endTime:this.state.timeRange.endTime
         }
       }
     }else{
         this.state = {
            timeRange: {
              name: '',
              startTime:'',
              endTime:''
              }
             }
       }
  }
  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;
     let stateCopy = Object.assign({}, this.state);
     stateCopy.timeRange[inputName] = inputValue;
     this.setState(stateCopy);
   }
  render() {


    return (
      <div className="TimeRange">
          name : <input type="text" name="name" value={this.state.timeRange.name} onChange={this.handleChange.bind(this)}/> &nbsp;
          startTime : <input type="text" name="startTime" value={this.state.timeRange.startTime} onChange={this.handleChange.bind(this)}/> &nbsp;
          endTime : <input type="text" name="endTime" value={this.state.timeRange.endTime} onChange={this.handleChange.bind(this)}/> &nbsp;
      </div>
    );
  }
}
export default TimeRange;
