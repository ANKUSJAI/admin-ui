import React, { Component } from 'react';
import ItemOption from './ItemOption';
import Money from './Money';
import AddOn from './AddOn';
import TimeTable from './TimeTable';

class Item extends Component {
  render() {

    let itemOptions;
    if(this.props.item.itemOptions){
     itemOptions = this.props.item.itemOptions.map(itemOption =>{
       return (
         <ItemOption key={itemOption.id} itemOption={itemOption}/>
       );
     });
    }
    let addOns;
    if(this.props.item.addOns){
     addOns = this.props.item.addOns.map(addOn =>{
       return (
         <AddOn key={addOn.id} addOn={addOn}/>
       );
     });
    }
    return (
      <div className="Item">
            <h1>Item- {this.props.item.name}</h1>
            id : <input type="id" name="id" value={this.props.item.id}/> &nbsp;
            name : <input type="text" name="name" value={this.props.item.name}/>
            <Money money={this.props.item.price} />
            description : <input type="text" name="description" value={this.props.item.description}/>
            prepTime : <input type="text" name="prepTime" value={this.props.item.prepTime}/>
            availability : <TimeTable openingHours={this.props.item.availability}/>
            displayOrder : <input type="text" name="description" value={this.props.item.displayOrder}/>
            active : <input type="text" name="active" value={this.props.item.active}/>
            calories: <input type="text" name="active" value={this.props.item.calories}/>
            {itemOptions}
            addOns: {addOns}
      </div>
    );
  }
}
export default Item;
