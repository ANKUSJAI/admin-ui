import React, { Component } from 'react';
import Money from './Money';

class ItemOption extends Component {
  constructor(props) {
     super(props);
     this.state = {
        itemOption: this.props.itemOption
         }
       }

  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;
     let stateCopy = Object.assign({}, this.state);
     stateCopy.itemOption[inputName] = inputValue;
     this.setState(stateCopy);
   }
  render() {
    return (
      <div className="ItemOption">
      <h1>Item Option- {this.props.ItemOption.type}</h1>
          type : <input type="type" name="type" value={this.state.itemOption.type} onChange={this.handleChange.bind(this)} /> &nbsp;
          <Money value={this.state.itemOption.money} />
      </div>
    );
  }
}
export default ItemOption;
