import React, {
  Component
} from 'react';
import Restraurant from './Restraurant';

class Restraurants extends Component {
  render() {
    let restraurants;
    if (this.props.restraurants) {
      restraurants = this.props.restraurants.map(restraurant => {
        return ( <Restraurant key = {restraurant.id} restraurant = {restraurant} /> );
      });
    }
    return ( <div className = "Restraurant">
               {restraurants}
            </div>);
  }
}
export default Restraurants;
