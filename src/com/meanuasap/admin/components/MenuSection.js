import React, { Component } from 'react';
import TimeTable from './TimeTable';
import Item from './Item';
import AddOn from './AddOn';

class MenuSection extends Component {

  constructor(props) {
     super(props);
     this.state = {
        menuSection: this.props.menuSection
         }
     }


  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;
     let stateCopy = Object.assign({}, this.state);
     stateCopy.menuSection[inputName] = inputValue;
     this.setState(stateCopy);
   }

  render() {
    let items = <Item />;
    if(this.props.menuSection.items !== null){
     items = this.props.menuSection.items.map(item =>{
       return (
         <Item key={item.id} item={item}/>
       );
     });
   }
   let addOns =  <AddOn/>;
   if(this.props.menuSection.addOns !== null){
    items = this.props.menuSection.addOns.map(addOn =>{
      return (
        <AddOn key={addOn.id} addOn={addOn}/>
      );
    });
  }
    return (
      <div className="MenuSection">
          <h1>Menu Section - {this.state.menuSection.name}</h1>
          id : <input type="id" name="id" value={this.state.menuSection.id} onChange={this.handleChange.bind(this)} /> &nbsp;
          name : <input type="text" name="name" value={this.state.menuSection.name} onChange={this.handleChange.bind(this)} /><br/><br/>
          description : <input type="text" name="description" value={this.state.menuSection.description} onChange={this.handleChange.bind(this)} />&nbsp;
          displayOrder : <input type="text" name="displayOrder" value={this.state.menuSection.displayOrder} onChange={this.handleChange.bind(this)} /><br/><br/>
          active : <input type="text" name="active" value={this.state.menuSection.active} onChange={this.handleChange.bind(this)} />&nbsp;
          tags: <input type="text" name="tags" value={this.state.menuSection.tags} onChange={this.handleChange.bind(this)} /><br/><br/>
          {items}
          {addOns}
          <TimeTable openingHours={this.props.menuSection.availability}/>
      </div>
    );
  }
}
export default MenuSection;
