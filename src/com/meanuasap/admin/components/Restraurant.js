import React, {
  Component
} from 'react';
import Address from './Address';
import TimeTable from './TimeTable'
import Menu from './Menu'
import '../../../../form.css';

class Restraurant extends Component {

  constructor(props) {
    super(props);
    this.state = {
      restraurant: this.props.restraurant
    }
  }
  handleChange(event) {
    let inputName = event.target.name;
    let inputValue = event.target.value;
    let stateCopy = Object.assign({}, this.state);
    stateCopy.restraurant[inputName] = inputValue;
    this.setState(stateCopy);
  }
  render() {
    let menus;
    if (this.props.restraurant.menus) {
      menus = this.props.restraurant.menus.map(menu => {
        return ( <Menu key = {menu.id} menu = {menu} /> );
      });
    }
    return ( <
      div className = "Restraurant" >
      <h1> Restraurant </h1>
      Id: <input type = "text" name = "id" value = {this.state.restraurant.id} onChange = {this.handleChange.bind(this)} /> &nbsp;
      Name: < input type = "text" name = "name" value = {this.state.restraurant.name} onChange = {this.handleChange.bind(this)}/> <br/ > < br / >
      Emails: < input type = "text" name = "emails" value = {this.state.restraurant.emails} onChange = {this.handleChange.bind(this)} /> &nbsp;
      Phone Numbers: < input type = "text" name = "phoneNumbers" value = {this.state.restraurant.phoneNumbers} onChange = {this.handleChange.bind(this)}/> <br/ > < br / >
      Web address: < input type = "text" name = "webAddress" value = {this.state.restraurant.webAddress} onChange = {this.handleChange.bind(this)}/>  &nbsp;
      Cuisines: < input type = "text" name = "cuisines" value = {this.state.restraurant.cuisines}  onChange = {this.handleChange.bind(this) } />  <br/ > < br / >
      Picture Id: < input type = "text" name = "pictureId" value = {this.state.restraurant.pictureId} onChange = {this.handleChange.bind(this)} />  &nbsp;
      Tags: < input type = "text" name = "tags" value = {this.state.restraurant.tags} onChange = {this.handleChange.bind(this)}/>  <br/ > < br / >
      Types: < input type = "text" name = "types" value = {this.state.restraurant.types} onChange = {this.handleChange.bind(this)} />&nbsp;
      <Address address = {this.state.restraurant.address}/>
      <TimeTable openingHours = {this.state.restraurant.openingHours}/>
      {menus}
      </div>
    );
  }
}
export default Restraurant;
