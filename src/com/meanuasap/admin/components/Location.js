import React, { Component } from 'react';

class Location extends Component {
  constructor(props) {
     super(props);
     this.state = {
        location: this.props.location
         }
       }

  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;
     let stateCopy = Object.assign({}, this.state);
     stateCopy.location[inputName] = inputValue;
     this.setState(stateCopy);
   }
  render() {
    return (
      <div className="Location"><br/>
          <h3>Map location </h3>
          latitude : <input type="text" name="latitude" value={this.state.location.latitude} onChange={this.handleChange.bind(this)} /> &nbsp;
          longitude : <input type="text" name="longitude" value={this.state.location.longitude} onChange={this.handleChange.bind(this)} />
      </div>
    );
  }
}
export default Location;
