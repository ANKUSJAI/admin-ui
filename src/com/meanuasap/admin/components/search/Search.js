import React, { Component } from 'react';
import $ from 'jquery';

class Search extends Component {

    constructor(){
      super();
      this.state = {
        restraurants : []
      }
    }
  getRestraurant(){
    $.ajax({
      url: 'http://menuasap.com:8080/1c63b769-814a-4fd8-9e07-7db9db9266ab',
      crossOrigin: true,
      type: "GET",
      dataType : 'json',
      accept: 'application/json',
      cache : false,
      success : function(data){
        this.setState({restraurants:data}, function(){
          this.props.callbackFromParent(this.state.restraurants);
          console.log(this.state);
        });
      }.bind(this),
      error: function(xhr, status, error){
          console.log(xhr);
          console.log(status);
          console.log(error);
      }
    });
    }
  /*componentWillMount(){
      console.log("componentWillMount");
    this.getRestraurant();
  }
  componentDidMount(){
      console.log("componentDidMount");
    this.getRestraurant();
  }*/
  handleSubmit(e){
    if(this.refs.id.value === '' && this.refs.name.value === ''){
      alert("please provide id or name");
    }else{
      this.getRestraurant();
    }
    e.preventDefault();
  }
  render() {
    return (
      <div className="Search">
         <form onSubmit={this.handleSubmit.bind(this)}><br/>
          <label>Id :</label>
          <input type="id" ref="id" /> &nbsp;
          <label>Name :</label>
          <input type="text" ref="name" />
          <input type="submit" value="Search"/>
        </form>
      </div>
    );
  }
}
export default Search;
