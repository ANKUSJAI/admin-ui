import React, { Component } from 'react';
import Money from './Money';

class AddOn extends Component {

 constructor(props) {
    super(props);
    if(this.props.addOn){
      this.state = {
         addOn: this.props.addOn
          }
        }else{
      this.state = {
         addOn: {
           id:'',
           name:'',
           description:'',
           displayOrder:'',
           price:''
           }
          }
        }
    }


 handleChange(event) {
    let inputName = event.target.name;
    let inputValue = event.target.value;
    let stateCopy = Object.assign({}, this.state);
    stateCopy.addOn[inputName] = inputValue;
    this.setState(stateCopy);
  }
  render() {
    return (
      <div className="AddOn">
          <h3>AddOn</h3>
          id : <input type="id" name="id" value={this.state.addOn.id} onChange={this.handleChange.bind(this)}/> &nbsp;
          name : <input type="text" name="name" value={this.state.addOn.name} onChange={this.handleChange.bind(this)}/> <br/><br/>
          description : <input type="text" name="description" value={this.state.addOn.description} onChange={this.handleChange.bind(this)} />&nbsp;
          displayOrder : <input type="text" name="displayOrder" value={this.state.addOn.displayOrder} onChange={this.handleChange.bind(this)}/><br/><br/>
          <Money money={this.state.addOn.price} />
      </div>
    );
  }
}
export default AddOn;
