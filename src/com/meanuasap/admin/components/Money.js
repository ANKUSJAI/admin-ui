import React, { Component } from 'react';

class Money extends Component {
  constructor(props) {
     super(props);
       if(this.props.money){
         this.state = {
            money: this.props.money
             }
       }else{
         this.state = {
            money: {
              amount : '',
              currency: ''
            }
             }
           }
       }


  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;

     let stateCopy = Object.assign({}, this.state);
     stateCopy.money[inputName] = inputValue;
     this.setState(stateCopy);
   }
  render() {
    return (
      <div className="Money">
          amount : <input type="text" name="amount" value={this.state.money.amount} onChange={this.handleChange.bind(this)}/> &nbsp;
          currency : <input type="text" name="currency" value={this.state.money.currency} onChange={this.handleChange.bind(this)}/>
      </div>
    );
  }
}
export default Money;
