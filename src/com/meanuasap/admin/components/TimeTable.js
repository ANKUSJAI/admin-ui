import React, { Component } from 'react';
import TimeRangeComponent from './TimeRangeComponent'
class TimeTable extends Component {
  constructor(props) {
     super(props);
     if(this.props.openingHours){
        this.state = {
          openingHours: {
             monday:this.props.openingHours.monday,
             tuesday:this.props.openingHours.tuesday,
             wednesday:this.props.openingHours.wednesday,
             thursday:this.props.openingHours.thursday,
             friday:this.props.openingHours.friday,
             saturday:this.props.openingHours.saturday,
             sunday:this.props.openingHours.sunday
        }
      }
    }else{
      this.state = {
        openingHours: {
           monday:'',
           tuesday:'',
           wednesday:'',
           thursday:'',
           friday:'',
           saturday:'',
           sunday:''
      }
    }
    }
  }

  render() {
    let day : '';
    let mondaySlots = <TimeRangeComponent  timeRanges={day}/>;
    let tuesdaySlots = <TimeRangeComponent  timeRanges={day}/>;
    let wednesdaySlots  = <TimeRangeComponent  timeRanges={day}/>;
    let thursdaySlots  = <TimeRangeComponent  timeRanges={day}/>;
    let fridaySlots  = <TimeRangeComponent  timeRanges={day}/>;
    let saturdaySlots  = <TimeRangeComponent  timeRanges={day}/>;
    let sundaySlots  = <TimeRangeComponent  timeRanges={day}/>;

     if( this.state.openingHours.monday){
       mondaySlots = this.state.openingHours.monday.map(timeRanges =>{
         return (
           <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
         );
       });
     }

     if( this.state.openingHours.tuesday){
      tuesdaySlots = this.state.openingHours.tuesday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
     }

     if( this.state.openingHours.wednesday){
      wednesdaySlots = this.state.openingHours.wednesday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
     }

     if( this.state.openingHours.thursday){
      thursdaySlots = this.state.openingHours.thursday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
     }

     if( this.state.openingHours.friday){
      fridaySlots = this.state.openingHours.friday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
     }

     if( this.state.openingHours.saturday){
      saturdaySlots = this.state.openingHours.saturday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
     }

     if( this.state.openingHours.sunday){
      sundaySlots = this.state.openingHours.sunday.map(timeRanges =>{
        return (
          <TimeRangeComponent key={timeRanges.name} timeRanges={timeRanges}/>
        );
      });
    }

    return (
      <div className="TimeTable">
          <h3>Time table</h3>
          Monday Opening Hours <br/> <br/>{mondaySlots}<br/> <br/>
          Tuesday Opening Hours <br/> <br/>{tuesdaySlots}<br/> <br/>
          Wednesday Opening Hours <br/><br/> {wednesdaySlots}<br/> <br/>
          Thursday Opening Hours <br/> <br/>{thursdaySlots}<br/> <br/>
          Friday Opening Hours <br/> <br/>{fridaySlots}<br/> <br/>
          Saturday Opening Hours <br/> <br/>{saturdaySlots}<br/> <br/>
          Sunday Opening Hours <br/> <br/>{sundaySlots}<br/> <br/>
      </div>
    );
  }
}
export default TimeTable;
