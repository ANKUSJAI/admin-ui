import React, { Component } from 'react';
import Location from './Location';

class Address extends Component {
  constructor(props) {
      super(props);
      this.state = {
        address: this.props.address
      }
    }
 handleChange(event) {
    let inputName = event.target.name;
    let inputValue = event.target.value;
    let stateCopy = Object.assign({}, this.state);
    stateCopy.address[inputName] = inputValue;
    this.setState(stateCopy);
  }
  render() {
    return (
      <div className="Address">
          <h3> Address </h3>
          Address Line : <textarea rows="4" cols="50" name="addressLine" value={this.state.address.addressLine} onChange={this.handleChange.bind(this)} />&nbsp;<br/> <br/>
          Area : <input type="text" name="area" value={this.state.address.area} onChange={this.handleChange.bind(this)} />&nbsp;
          City : <input type="text" name="city" value={this.state.address.city} onChange={this.handleChange.bind(this)} />&nbsp;<br/> <br/>
          State : <input type="text" name="state" value={this.state.address.state} onChange={this.handleChange.bind(this)} />&nbsp;
          Zip code : <input type="text" name="zipCode" value={this.state.address.zipCode} onChange={this.handleChange.bind(this)} />&nbsp;<br/> <br/>
          Country : <input type="text" name="country" value={this.state.address.country} onChange={this.handleChange.bind(this)} />
          <Location location={this.state.address.location}/><br/> <br/>
      </div>
    );
  }
}
export default Address;
