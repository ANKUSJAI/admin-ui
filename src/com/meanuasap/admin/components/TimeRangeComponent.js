import React, { Component } from 'react';

class TimeRangeComponent extends Component {
  constructor(props) {
  super(props);
  if(this.props.timeRanges !== undefined){
    console.log(this.props.timeRanges);
    this.state = {
      timeRanges : this.props.timeRanges
    }
  }else{
    this.state = {
      timeRanges : [{
        name : '',
        startTime :'',
        endTime : ''
      }]
    }
  }
  }
  handleNewRowSubmit(newTimeRange) {
    alert("ANK");
    alert(newTimeRange);
console.log(this.props.timeRanges);
console.log(this.state.timeRanges);

    this.setState( {timeRanges: this.props.timeRanges.concat([newTimeRange])} );
   }
  render() {
    var tableStyle = {width: '100%'};
    var leftTdStyle = {width: '50%',padding:'20px',verticalAlign: 'top'};
    var rightTdStyle = {width: '50%',padding:'20px',verticalAlign: 'top'};

   console.log("render");
   console.log(this.state.timeRanges);
    return (
          <div>
            <table style={tableStyle}>
                <tr>
                  <td style={leftTdStyle}>
                    <TimeRangeList timeRanges={this.state.timeRanges}/>
                  </td>
                  <td style={rightTdStyle}>
                    <NewRow onRowSubmit={this.handleNewRowSubmit}/>
                  </td>
                </tr>
            </table>
          </div>
    );
  }


}
// time Range list

export class TimeRangeList extends React.Component{
  constructor(props){
    super(props);
    console.log(this.props);
    if(this.props.timeRanges !== undefined){
      this.state = {
        timeRanges : this.props.timeRanges
      }
    }else{
      this.state = {
        timeRanges : [{
          name : '',
          startTime :'',
          endTime : ''
        }]
      }
    }
  }
       render() {
         console.log("in render");
         console.log(this.state.timeRanges);
         var timeRanges = this.state.timeRanges.map(function (timeRanges) {
                       return (
                           <TimeRange timeRange={timeRanges}/>
                       );
                     });
         return (
           <div>
             <h3>List of Time Range</h3>
             <table className="table table-striped">
               <th>Name</th>
               <th>Start Time</th>
               <th>End Time</th>
               <th>Action</th>
               {timeRanges}
             </table>
           </div>
           );
       }
     };

// time range
export class TimeRange extends React.Component{
  constructor(props){
    super(props);
    console.log(this.props);
    if(this.props.timeRange !== undefined){
      this.state = {
        timeRange : this.props.timeRange
      }
    }else{
      this.state = {
        timeRange : [{
          name : '',
          startTime :'',
          endTime : ''
        }]
      }
    }
  }
     handleRemove() {
       console.log( "removing..." );
     }
     render() {
       console.log(this.state.timeRange.name);
       return (
         <tr>
           <td>{this.state.timeRange.name}</td>
           <td>{this.state.timeRange.startTime}</td>
           <td>{this.state.timeRange.EndTime}</td>
           <td><input type="button"  className="btn btn-primary" value="Remove" onClick={this.handleRemove}/></td>
         </tr>
         );
     }
   };

// new row
export class NewRow extends React.Component{
    handleSubmit(e) {
      alert("a");
      alert(this.refs);
      alert(this.refs.name);

      var name = this.refs.name.value;
      alert(name);
      var startTime = this.refs.startTime.value;
      alert(startTime);

      var endTime = this.refs.endTime.value;
      alert(endTime);

      var newrow = {name: name, startTime: startTime, endTime: endTime };
      alert(newrow);

      this.props.onRowSubmit( newrow );

      this.refs.name.value = '';
      this.refs.startTime.getDOMNode().value = '';
      this.refs.endTime.getDOMNode().value = '';
      e.preventDefault();

    }
    render(){
      console.log("in new row");
      var inputStyle = {padding:'12px'}
      return (
        <div className="well">
          <h3>Add A Time Range</h3>
        <form onSubmit={this.handleSubmit.bind(this)} >
          <div className="input-group input-group-lg" style={inputStyle}>
            <input type="text"  className="form-control col-md-8"  placeholder="Name" ref="name"/>
          </div>
          <div className="input-group input-group-lg" style={inputStyle}>
            <input type="text"  className="form-control col-md-8" placeholder="Start Time" ref="startTime"/>
          </div>
          <div className="input-group input-group-lg" style={inputStyle}>
            <input type="text"  className="form-control col-md-8" placeholder="End Time" ref="endTime"/>
          </div>
          <div className="input-group input-group-lg" style={inputStyle}>
            <input type="submit"  className="btn btn-primary" value="Add Time Range"/>
          </div>
        </form>
        </div>
        );
      }
  };
export default TimeRangeComponent;
