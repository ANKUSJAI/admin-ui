import React, { Component } from 'react';
import TimeTable from './TimeTable';
import MenuSection from './MenuSection';
class Menu extends Component {
  constructor(props) {
     super(props);
     this.state = {
        menu: this.props.menu
         }
       }

  handleChange(event) {
     let inputName = event.target.name;
     let inputValue = event.target.value;
     let stateCopy = Object.assign({}, this.state);
     stateCopy.menu[inputName] = inputValue;
     this.setState(stateCopy);
   }
  render() {
    let menusections;
    if(this.props.menu.menuSections){
     menusections = this.props.menu.menuSections.map(menuSection =>{
       return (
         <MenuSection key={menuSection.id} menuSection={menuSection}/>
       );
     });
    }
    return (
      <div className="Menu">
            <h1>MENUS - {this.state.menu.name}</h1>
            Id : <input type="id" name="id" value={this.state.menu.id} onChange={this.handleChange.bind(this)} /> &nbsp;
            Name : <input type="text" name="name" value={this.state.menu.name} onChange={this.handleChange.bind(this)} /> <br/><br/>
            Description : <input type="text" name="description" value={this.state.menu.description} onChange={this.handleChange.bind(this)} />&nbsp;
            Active : <input type="text" name="active" value={this.state.menu.active} onChange={this.handleChange.bind(this)} /><br/><br/>
            DisplayOrder : <input type="text" name="displayOrder" value={this.state.menu.displayOrder} onChange={this.handleChange.bind(this)} />&nbsp;
            Tags:  <input type="text" name="tags" value={this.state.menu.tags} onChange={this.handleChange.bind(this)} />
            <TimeTable openingHours={this.props.menu.availability}/> <br/>
            {menusections}
      </div>
    );
  }
}
export default Menu;
